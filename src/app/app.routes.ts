import { Component } from "@angular/core";
import { RouterModule, Routes } from "@angular/router";

import { DesayunoComponent } from "./componentes/desayuno/desayuno.component";
import { AlmuerzoComponent } from "./componentes/almuerzo/almuerzo.component";
import { CenaComponent } from "./componentes/cena/cena.component";
import { BebidaComponent } from "./componentes/bebida/bebida.component";
import { MenuComponent } from "./componentes/menu/menu.component";
import { PostreComponent } from "./componentes/postre/postre.component";

const app_routes: Routes = [
{ path: 'desayuno', component: DesayunoComponent},
{ path: 'almuerzo', component: AlmuerzoComponent},
{ path: 'cena', component: CenaComponent},
{ path: 'bebida', component: BebidaComponent},
{ path: 'menu', component: MenuComponent},
{ path: 'postre', component: PostreComponent},
{ path: '**',pathMatch: 'full', redirectTo: 'menu'}
];

export const app_routing = RouterModule.forRoot(app_routes);