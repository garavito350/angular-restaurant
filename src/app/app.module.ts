import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

//rutas
import { app_routing } from './app.routes';

import { AppComponent } from './app.component';
import { DesayunoComponent } from './componentes/desayuno/desayuno.component';
import { AlmuerzoComponent } from './componentes/almuerzo/almuerzo.component';
import { BebidaComponent } from './componentes/bebida/bebida.component';
import { MenuComponent } from './componentes/menu/menu.component';
import { CenaComponent } from './componentes/cena/cena.component';
import { PostreComponent } from './componentes/postre/postre.component';

@NgModule({
  declarations: [
    AppComponent,
    DesayunoComponent,
    AlmuerzoComponent,
    BebidaComponent,
    MenuComponent,
    CenaComponent,
    PostreComponent
  ],
  imports: [
    BrowserModule,
    app_routing
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
